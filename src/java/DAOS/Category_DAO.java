package DAOS;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import Base_DAOS.Category_DAO_BASE;
import Entities.Category;
import Util.Notification;
import Util.SystemSetting;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;
public class Category_DAO extends Category_DAO_BASE  {
 public String validateRecord(Category recObject) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        String ERR_MSG = SystemSetting.STATUS_SUCCESS;

  List<Category> list = this.getList("name='" + recObject.getName() + "'");

        if (list != null && list.size() > 0 && recObject.getRowId() == null) {
            ERR_MSG += "Invalid Name, Name already exists" +"\n";
        }
         return ERR_MSG;
 }
}
