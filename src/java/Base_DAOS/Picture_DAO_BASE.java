package Base_DAOS;

import Entities.Picture;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.math.*;
import Util.DAO_Generator;
import java.util.List;
public class Picture_DAO_BASE {
 //-----GET RECORD METHOD----------- 
 public Picture getRecord(String rowID) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {Picture entity_record  = null;
        Connection con = DAO_Generator.getConnection();
        String query = "select * from picture where row_id = '" + rowID + "'";
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
entity_record = new Picture();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setProductId(rs.getString("Product_Id")); 
entity_record.setPath(rs.getString("Path")); 
        }
       
            rs.close();
            stmt.close();
            
            return entity_record;
}
//-----------------INSERT METHOD ---------------
  public void addRecord(Picture record) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException { 
 record.setRowId(DAO_Generator.generateRowID());
Connection con = DAO_Generator.getConnection(); 
 PreparedStatement ps = con.prepareStatement("INSERT INTO picture VALUES(?,?,?)");
ps.setString(1,record.getRowId()); 
ps.setString(2,record.getProductId()); 
ps.setString(3,record.getPath()); 

 int i = ps.executeUpdate();
        ps.close();
        
}
//--------- DELETE METHOD--------
 public void deleteRecord(String record_ID) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {   Connection con = DAO_Generator.getConnection();
            Statement stmt = con.createStatement();

            int i = stmt.executeUpdate("DELETE FROM picture WHERE row_id='" + record_ID+"'");
            stmt.close();
            
        }
//--------- UPDATE METHOD--------
  public void updateRecord(Picture record) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException { Connection con = DAO_Generator.getConnection();
            PreparedStatement ps = con.prepareStatement("UPDATE picture SET row_id = ?, Product_Id = ?, Path = ? WHERE row_id=? ");
ps.setString(1,record.getRowId()); 
ps.setString(2,record.getProductId()); 
ps.setString(3,record.getPath()); 
ps.setString(4,record.getRowId()); 
   int i = ps.executeUpdate();
            ps.close();
            
}
//--------- LIST METHOD ALL RECORDS--------
public List<Picture> getList() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
  List<Picture> listOfRecords = new ArrayList<>();
        Connection con = DAO_Generator.getConnection();
        String query = "select * from picture";
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
Picture entity_record = new Picture();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setProductId(rs.getString("Product_Id")); 
entity_record.setPath(rs.getString("Path")); 
        listOfRecords.add(entity_record);
 }
       
rs.close();
        stmt.close();
        
        return listOfRecords;
}
//--------- LIST METHOD WHERE STATEMENT--------
public List<Picture> getList(String myQuery) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
  List<Picture> listOfRecords = new ArrayList<>();
        Connection con = DAO_Generator.getConnection();
        String query = "select * from picture where " + myQuery;
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
Picture entity_record = new Picture();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setProductId(rs.getString("Product_Id")); 
entity_record.setPath(rs.getString("Path")); 
        listOfRecords.add(entity_record);
 }
       
rs.close();
        stmt.close();
        
        return listOfRecords;
}


 }