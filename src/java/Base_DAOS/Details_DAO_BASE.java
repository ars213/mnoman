package Base_DAOS;

import Entities.Details;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.math.*;
import Util.DAO_Generator;
import java.util.List;
public class Details_DAO_BASE {
 //-----GET RECORD METHOD----------- 
 public Details getRecord(String rowID) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {Details entity_record  = null;
        Connection con = DAO_Generator.getConnection();
        String query = "select * from details where row_id = '" + rowID + "'";
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
entity_record = new Details();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setProductId(rs.getString("Product_Id")); 
entity_record.setLable(rs.getString("Lable")); 
entity_record.setValue(rs.getBigDecimal("Value"));
        }
       
            rs.close();
            stmt.close();
            
            return entity_record;
}
//-----------------INSERT METHOD ---------------
  public void addRecord(Details record) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException { 
 record.setRowId(DAO_Generator.generateRowID());
Connection con = DAO_Generator.getConnection(); 
 PreparedStatement ps = con.prepareStatement("INSERT INTO details VALUES(?,?,?,?)");
ps.setString(1,record.getRowId()); 
ps.setString(2,record.getProductId()); 
ps.setString(3,record.getLable()); 
ps.setBigDecimal(4,record.getValue()); 

 int i = ps.executeUpdate();
        ps.close();
        
}
//--------- DELETE METHOD--------
 public void deleteRecord(String record_ID) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {   Connection con = DAO_Generator.getConnection();
            Statement stmt = con.createStatement();

            int i = stmt.executeUpdate("DELETE FROM details WHERE row_id='" + record_ID+"'");
            stmt.close();
            
        }
//--------- UPDATE METHOD--------
  public void updateRecord(Details record) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException { Connection con = DAO_Generator.getConnection();
            PreparedStatement ps = con.prepareStatement("UPDATE details SET row_id = ?, Product_Id = ?, Lable = ?, Value = ? WHERE row_id=? ");
ps.setString(1,record.getRowId()); 
ps.setString(2,record.getProductId()); 
ps.setString(3,record.getLable()); 
ps.setBigDecimal(4,record.getValue()); 
ps.setString(5,record.getRowId()); 
   int i = ps.executeUpdate();
            ps.close();
            
}
//--------- LIST METHOD ALL RECORDS--------
public List<Details> getList() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
  List<Details> listOfRecords = new ArrayList<>();
        Connection con = DAO_Generator.getConnection();
        String query = "select * from details";
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
Details entity_record = new Details();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setProductId(rs.getString("Product_Id")); 
entity_record.setLable(rs.getString("Lable")); 
entity_record.setValue(rs.getBigDecimal("Value"));
        listOfRecords.add(entity_record);
 }
       
rs.close();
        stmt.close();
        
        return listOfRecords;
}
//--------- LIST METHOD WHERE STATEMENT--------
public List<Details> getList(String myQuery) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
  List<Details> listOfRecords = new ArrayList<>();
        Connection con = DAO_Generator.getConnection();
        String query = "select * from details where " + myQuery;
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
Details entity_record = new Details();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setProductId(rs.getString("Product_Id")); 
entity_record.setLable(rs.getString("Lable")); 
entity_record.setValue(rs.getBigDecimal("Value"));
        listOfRecords.add(entity_record);
 }
       
rs.close();
        stmt.close();
        
        return listOfRecords;
}


 }