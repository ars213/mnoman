package Base_DAOS;

import Entities.Users;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.math.*;
import Util.DAO_Generator;
import java.util.List;
public class Users_DAO_BASE {
 //-----GET RECORD METHOD----------- 
 public Users getRecord(String rowID) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {Users entity_record  = null;
        Connection con = DAO_Generator.getConnection();
        String query = "select * from users where row_id = '" + rowID + "'";
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
entity_record = new Users();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setFirstName(rs.getString("first_name")); 
entity_record.setLastName(rs.getString("last_name")); 
entity_record.setUserName(rs.getString("user_name")); 
entity_record.setPassword(rs.getString("password")); 
entity_record.setEmail(rs.getString("email")); 
        }
       
            rs.close();
            stmt.close();
            
            return entity_record;
}
//-----------------INSERT METHOD ---------------
  public void addRecord(Users record) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException { 
 record.setRowId(DAO_Generator.generateRowID());
Connection con = DAO_Generator.getConnection(); 
 PreparedStatement ps = con.prepareStatement("INSERT INTO users VALUES(?,?,?,?,?,?,?)");
ps.setString(1,record.getRowId()); 
ps.setString(2,record.getFirstName()); 
ps.setString(3,record.getLastName()); 
ps.setString(4,record.getUserName()); 
ps.setString(5,record.getPassword()); 
ps.setString(6,record.getEmail()); 

 int i = ps.executeUpdate();
        ps.close();
        
}
//--------- DELETE METHOD--------
 public void deleteRecord(String record_ID) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {   Connection con = DAO_Generator.getConnection();
            Statement stmt = con.createStatement();

            int i = stmt.executeUpdate("DELETE FROM users WHERE row_id='" + record_ID+"'");
            stmt.close();
            
        }
//--------- UPDATE METHOD--------
  public void updateRecord(Users record) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException { Connection con = DAO_Generator.getConnection();
            PreparedStatement ps = con.prepareStatement("UPDATE users SET row_id = ?, first_name = ?, last_name = ?, user_name = ?, password = ?, email = ?, user_type = ? WHERE row_id=? ");
ps.setString(1,record.getRowId()); 
ps.setString(2,record.getFirstName()); 
ps.setString(3,record.getLastName()); 
ps.setString(4,record.getUserName()); 
ps.setString(5,record.getPassword()); 
ps.setString(6,record.getEmail()); 
ps.setString(7,record.getRowId()); 
   int i = ps.executeUpdate();
            ps.close();
            
}
//--------- LIST METHOD ALL RECORDS--------
public List<Users> getList() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
  List<Users> listOfRecords = new ArrayList<>();
        Connection con = DAO_Generator.getConnection();
        String query = "select * from users";
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
Users entity_record = new Users();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setFirstName(rs.getString("first_name")); 
entity_record.setLastName(rs.getString("last_name")); 
entity_record.setUserName(rs.getString("user_name")); 
entity_record.setPassword(rs.getString("password")); 
entity_record.setEmail(rs.getString("email")); 
        listOfRecords.add(entity_record);
 }
       
rs.close();
        stmt.close();
        
        return listOfRecords;
}
//--------- LIST METHOD WHERE STATEMENT--------
public List<Users> getList(String myQuery) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
  List<Users> listOfRecords = new ArrayList<>();
        Connection con = DAO_Generator.getConnection();
        String query = "select * from users where " + myQuery;
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
Users entity_record = new Users();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setFirstName(rs.getString("first_name")); 
entity_record.setLastName(rs.getString("last_name")); 
entity_record.setUserName(rs.getString("user_name")); 
entity_record.setPassword(rs.getString("password")); 
entity_record.setEmail(rs.getString("email")); 
        listOfRecords.add(entity_record);
 }
       
rs.close();
        stmt.close();
        
        return listOfRecords;
}


 }