package Base_DAOS;

import Entities.Category;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.math.*;
import Util.DAO_Generator;
import java.util.List;
public class Category_DAO_BASE {
 //-----GET RECORD METHOD----------- 
 public Category getRecord(String rowID) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {Category entity_record  = null;
        Connection con = DAO_Generator.getConnection();
        String query = "select * from category where row_id = '" + rowID + "'";
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
entity_record = new Category();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setName(rs.getString("Name")); 
        }
       
            rs.close();
            stmt.close();
            
            return entity_record;
}
//-----------------INSERT METHOD ---------------
  public void addRecord(Category record) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException { 
 record.setRowId(DAO_Generator.generateRowID());
Connection con = DAO_Generator.getConnection(); 
 PreparedStatement ps = con.prepareStatement("INSERT INTO category VALUES(?,?)");
ps.setString(1,record.getRowId()); 
ps.setString(2,record.getName()); 

 int i = ps.executeUpdate();
        ps.close();
        
}
//--------- DELETE METHOD--------
 public void deleteRecord(String record_ID) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {   Connection con = DAO_Generator.getConnection();
            Statement stmt = con.createStatement();

            int i = stmt.executeUpdate("DELETE FROM category WHERE row_id='" + record_ID+"'");
            stmt.close();
            
        }
//--------- UPDATE METHOD--------
  public void updateRecord(Category record) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException { Connection con = DAO_Generator.getConnection();
            PreparedStatement ps = con.prepareStatement("UPDATE category SET row_id = ?, Name = ? WHERE row_id=? ");
ps.setString(1,record.getRowId()); 
ps.setString(2,record.getName()); 
ps.setString(3,record.getRowId()); 
   int i = ps.executeUpdate();
            ps.close();
            
}
//--------- LIST METHOD ALL RECORDS--------
public List<Category> getList() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
  List<Category> listOfRecords = new ArrayList<>();
        Connection con = DAO_Generator.getConnection();
        String query = "select * from category";
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
Category entity_record = new Category();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setName(rs.getString("Name")); 
        listOfRecords.add(entity_record);
 }
       
rs.close();
        stmt.close();
        
        return listOfRecords;
}
//--------- LIST METHOD WHERE STATEMENT--------
public List<Category> getList(String myQuery) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
  List<Category> listOfRecords = new ArrayList<>();
        Connection con = DAO_Generator.getConnection();
        String query = "select * from category where " + myQuery;
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
Category entity_record = new Category();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setName(rs.getString("Name")); 
        listOfRecords.add(entity_record);
 }
       
rs.close();
        stmt.close();
        
        return listOfRecords;
}


 }