package Base_DAOS;

import Entities.Products;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.math.*;
import Util.DAO_Generator;
import java.util.List;
public class Products_DAO_BASE {
 //-----GET RECORD METHOD----------- 
 public Products getRecord(String rowID) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {Products entity_record  = null;
        Connection con = DAO_Generator.getConnection();
        String query = "select * from products where row_id = '" + rowID + "'";
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
entity_record = new Products();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setName(rs.getString("Name")); 
entity_record.setPictuer(rs.getString("pictuer")); 
entity_record.setPrice(rs.getBigDecimal("Price"));
entity_record.setStatus(rs.getString("Status")); 
entity_record.setCategoryId(rs.getString("Category_Id")); 
entity_record.setPriceAfter(rs.getBigDecimal("Price_After"));
entity_record.setPriceFlg(rs.getString("Price_Flg"));
        }
       
            rs.close();
            stmt.close();
            
            return entity_record;
}
//-----------------INSERT METHOD ---------------
  public void addRecord(Products record) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException { 
 record.setRowId(DAO_Generator.generateRowID());
Connection con = DAO_Generator.getConnection(); 
 PreparedStatement ps = con.prepareStatement("INSERT INTO products VALUES(?,?,?,?,?,?,?,?)");
ps.setString(1,record.getRowId()); 
ps.setString(2,record.getName()); 
ps.setString(3,record.getPictuer()); 
ps.setBigDecimal(4,record.getPrice()); 
ps.setString(5,record.getStatus()); 
ps.setString(6,record.getCategoryId()); 
ps.setString(7,record.getPriceFlg()); 
ps.setBigDecimal(8,record.getPriceAfter()); 



 int i = ps.executeUpdate();
        ps.close();
        
}
//--------- DELETE METHOD--------
 public void deleteRecord(String record_ID) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {   Connection con = DAO_Generator.getConnection();
            Statement stmt = con.createStatement();

            int i = stmt.executeUpdate("DELETE FROM products WHERE row_id='" + record_ID+"'");
            stmt.close();
            
        }
//--------- UPDATE METHOD--------
  public void updateRecord(Products record) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException { Connection con = DAO_Generator.getConnection();
            PreparedStatement ps = con.prepareStatement("UPDATE products SET row_id = ?, Name = ?, pictuer = ?, Price = ?, Status = ?, Category_Id = ?, Price_Flg = ?, Price_After = ? WHERE row_id=? ");
ps.setString(1,record.getRowId()); 
ps.setString(2,record.getName()); 
ps.setString(3,record.getPictuer()); 
ps.setBigDecimal(4,record.getPrice()); 
ps.setString(5,record.getStatus()); 
ps.setString(6,record.getCategoryId()); 
ps.setString(7,record.getPriceFlg()); 
ps.setBigDecimal(8,record.getPriceAfter()); 
ps.setString(9,record.getRowId()); 
   int i = ps.executeUpdate();
            ps.close();
            
}
//--------- LIST METHOD ALL RECORDS--------
public List<Products> getList() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
  List<Products> listOfRecords = new ArrayList<>();
        Connection con = DAO_Generator.getConnection();
        String query = "select * from products";
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
Products entity_record = new Products();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setName(rs.getString("Name")); 
entity_record.setPictuer(rs.getString("pictuer")); 
entity_record.setPrice(rs.getBigDecimal("Price"));
entity_record.setStatus(rs.getString("Status")); 
entity_record.setCategoryId(rs.getString("Category_Id")); 
entity_record.setPriceAfter(rs.getBigDecimal("Price_After"));
entity_record.setPriceFlg(rs.getString("Price_Flg"));
        listOfRecords.add(entity_record);
 }
       
rs.close();
        stmt.close();
        
        return listOfRecords;
}
//--------- LIST METHOD WHERE STATEMENT--------
public List<Products> getList(String myQuery) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
  List<Products> listOfRecords = new ArrayList<>();
        Connection con = DAO_Generator.getConnection();
        String query = "select * from products where " + myQuery;
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
Products entity_record = new Products();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setName(rs.getString("Name")); 
entity_record.setPictuer(rs.getString("pictuer")); 
entity_record.setPrice(rs.getBigDecimal("Price"));
entity_record.setStatus(rs.getString("Status")); 
entity_record.setCategoryId(rs.getString("Category_Id")); 
entity_record.setPriceAfter(rs.getBigDecimal("Price_After"));
entity_record.setPriceFlg(rs.getString("Price_Flg"));
        listOfRecords.add(entity_record);
 }
       
rs.close();
        stmt.close();
        
        return listOfRecords;
}


 }