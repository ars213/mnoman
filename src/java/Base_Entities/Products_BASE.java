package Base_Entities;
import java.util.ArrayList;
import java.util.Date;
import java.math.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import Util.DAO_Generator;

public class Products_BASE implements java.io.Serializable {
 


//-----------------Variables ---------------
private String RowId; 
private String Name; 
private String Pictuer; 
private BigDecimal Price; 
private String Status; 
private String CategoryId; 
private BigDecimal PriceAfter; 
private String PriceFlg; 

    public Products_BASE() {
    }

//--------- Getters and setters --------
 public String getRowId() {
return this.RowId;}




  public void setRowId(String RowId) {


this.RowId = RowId;}


 public String getName() {
return this.Name;}




  public void setName(String Name) {


this.Name = Name;}


 public String getPictuer() {
return this.Pictuer;}




  public void setPictuer(String Pictuer) {


this.Pictuer = Pictuer;}


 public BigDecimal getPrice() {
return this.Price;}


  public void setPrice(BigDecimal Price) {


this.Price = Price;}


 public String getStatus() {
return this.Status;}




  public void setStatus(String Status) {


this.Status = Status;}


 public String getCategoryId() {
return this.CategoryId;}




  public void setCategoryId(String CategoryId) {


this.CategoryId = CategoryId;}


 public BigDecimal getPriceAfter() {
return this.PriceAfter;}


  public void setPriceAfter(BigDecimal PriceAfter) {


this.PriceAfter = PriceAfter;}

    public String getPriceFlg() {
        return PriceFlg;
    }

    public void setPriceFlg(String PriceFlg) {
        this.PriceFlg = PriceFlg;
    }


 

}