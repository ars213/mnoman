package Base_Entities;
import java.util.ArrayList;
import java.util.Date;
import java.math.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import Util.DAO_Generator;

public class Users_BASE implements java.io.Serializable {
 


//-----------------Variables ---------------
private String RowId; 
private String FirstName; 
private String LastName; 
private String UserName; 
private String Password; 
private String Email; 

    public Users_BASE() {
    }

//--------- Getters and setters --------
 public String getRowId() {
return this.RowId;}




  public void setRowId(String RowId) {


this.RowId = RowId;}


 public String getFirstName() {
return this.FirstName;}




  public void setFirstName(String FirstName) {


this.FirstName = FirstName;}


 public String getLastName() {
return this.LastName;}




  public void setLastName(String LastName) {


this.LastName = LastName;}


 public String getUserName() {
return this.UserName;}




  public void setUserName(String UserName) {


this.UserName = UserName;}


 public String getPassword() {
return this.Password;}




  public void setPassword(String Password) {


this.Password = Password;}


 public String getEmail() {
return this.Email;}




  public void setEmail(String Email) {


this.Email = Email;}


 

}