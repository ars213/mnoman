package Base_Entities;
import java.util.ArrayList;
import java.util.Date;
import java.math.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import Util.DAO_Generator;

public class Details_BASE implements java.io.Serializable {
 


//-----------------Variables ---------------
private String RowId; 
private String ProductId; 
private String Lable; 
private BigDecimal Value; 

    public Details_BASE() {
    }

//--------- Getters and setters --------
 public String getRowId() {
return this.RowId;}




  public void setRowId(String RowId) {


this.RowId = RowId;}


 public String getProductId() {
return this.ProductId;}




  public void setProductId(String ProductId) {


this.ProductId = ProductId;}


 public String getLable() {
return this.Lable;}




  public void setLable(String Lable) {


this.Lable = Lable;}


 public BigDecimal getValue() {
return this.Value;}


  public void setValue(BigDecimal Value) {


this.Value = Value;}


 

}