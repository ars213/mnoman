/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import javax.faces.bean.ManagedBean;

/**
 *
 * @author Raghad
 */
@ManagedBean
public class General {
    public static final String COLOR_NEUTRAL = "#808080";
    public static final String COLOR_ERROR = "#FFBABA";
    public static final String COLOR_SUCCESS = "#DFF2BF";
    public static final String COLOR_WARNING = "#FEEFB3";

    public static final String RECORD_SAVED_SUCCESS = "Record Saved Successfully!";
    public static final String RECORD_UPDATED_SUCCESS = "Record Updated Successfully!";
    public static final String RECORD_DELETED_SUCCESS = "Record Deleted Successfully!";

    public static final String EMP_GEN_REPORT_GRP_BY_GENERAL = "EGR_GNL";
    public static final String EMP_GEN_REPORT_GRP_BY_ORG = "EGR_ORG";
    public static final String EMP_GEN_REPORT_GRP_BY_WRK_PLACE = "EGR_WKP";

    public String getRECORD_SAVED_SUCCESS() {
        return RECORD_SAVED_SUCCESS;
    }

    public String getRECORD_UPDATED_SUCCESS() {
        return RECORD_UPDATED_SUCCESS;
    }

    public static String getRECORD_DELETED_SUCCESS() {
        return RECORD_DELETED_SUCCESS;
    }

    public String getEMP_GEN_REPORT_GRP_BY_GENERAL() {
        return EMP_GEN_REPORT_GRP_BY_GENERAL;
    }

    public String getEMP_GEN_REPORT_GRP_BY_ORG() {
        return EMP_GEN_REPORT_GRP_BY_ORG;
    }

    public String getEMP_GEN_REPORT_GRP_BY_WRK_PLACE() {
        return EMP_GEN_REPORT_GRP_BY_WRK_PLACE;
    }

    public String getCOLOR_NEUTRAL() {
        return COLOR_NEUTRAL;
    }

    public String getCOLOR_ERROR() {
        return COLOR_ERROR;
    }

    public String getCOLOR_SUCCESS() {
        return COLOR_SUCCESS;
    }

    public String getCOLOR_WARNING() {
        return COLOR_WARNING;
    }

    
}
