/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author Mohammad Nairat
 */
public class FiveSecondsObject {

    Integer hours;
    Integer minutes;
    Integer seconds;

    Integer fiveSecondFormat;

    public FiveSecondsObject() {
    }

    public FiveSecondsObject(Integer fiveSeconds) {
        this.setFiveSecondFormat(fiveSeconds);
    }

    public FiveSecondsObject(Integer hours, Integer minutes, Integer seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
        this.fiveSecondFormat = hours * 24 * 60 + minutes * 60 + seconds;

    }

    public FiveSecondsObject(Date date) {
        if (date == null) {
            return;
        }
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        this.hours = cal.get(Calendar.HOUR_OF_DAY);
        this.minutes = cal.get(Calendar.MINUTE);
        this.seconds = cal.get(Calendar.SECOND);
        this.fiveSecondFormat = hours * 24 * 60 + minutes * 60 + seconds;

    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }

    public Integer getSeconds() {
        return seconds;
    }

    public void setSeconds(Integer seconds) {
        this.seconds = seconds;
    }

    public Integer getFiveSecondFormat() {
        this.fiveSecondFormat = hours * 60 * 60 + minutes * 60 + seconds;
        return fiveSecondFormat;
    }

    public void setFiveSecondFormat(Integer fiveSecondFormat) {
        if (fiveSecondFormat == null) {
            return;
        }
        this.fiveSecondFormat = fiveSecondFormat;
        this.hours = fiveSecondFormat / 3600;
        int extra = fiveSecondFormat % 3600;
        this.minutes = extra / 60;
        this.seconds = minutes % 60;

    }

    public Date toDate() {
        Date date;
        Calendar cal = new GregorianCalendar(1970, Calendar.JANUARY, 1, hours, minutes, seconds);
        date = new Date(cal.getTimeInMillis());
        return date;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        if (hours < 10) {
            sb.append(0);
        }
        sb.append(hours);
        sb.append(":");

        if (minutes < 10) {
            sb.append(0);
        }
        sb.append(minutes);

        return sb.toString();
    }

}
