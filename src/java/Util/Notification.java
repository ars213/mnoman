/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Raghad
 */
@ViewScoped
@ManagedBean
public class Notification {
    public static void showNotification(String title, String msg) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("mainNotificationForSUC", new FacesMessage(FacesMessage.SEVERITY_INFO,
                title, msg));
    }

    public static void showWarning(String title, String msg) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("mainNotificationForSUC", new FacesMessage(FacesMessage.SEVERITY_WARN,
                title, msg));
    }

    public static void showError(String title, String msg) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("mainNotificationForSUC", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                title, msg));
    }

    public static void showError(Exception e) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("mainNotificationFor", new FacesMessage(FacesMessage.SEVERITY_FATAL,
                "Errors found in logic!", "Error:\n" + e.getMessage() + "\nCaused By:\n" + e.getCause()));
        FacesContext.getCurrentInstance().validationFailed();
        e.printStackTrace();
    }
}
