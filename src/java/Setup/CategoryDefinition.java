/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Setup;

import DAOS.Category_DAO;
import Entities.Category;
import Util.General;
import Util.Notification;
import Util.SystemSetting;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Raghad
 */
@ManagedBean
@SessionScoped
public class CategoryDefinition {
    Category currentCategory=new Category();
    List<Category> listOfAllCateory=new ArrayList<>();
    Category_DAO categoryBo=new Category_DAO();
    public void init() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException{
        currentCategory=new Category();
        categoryBo=new Category_DAO();
        listOfAllCateory=new Category_DAO().getList();
    }
    public void saveCategory() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        String callback = categoryBo.validateRecord(currentCategory);
            if (callback.equals(SystemSetting.STATUS_SUCCESS)) {
                if(currentCategory.getRowId()!=null){
                categoryBo.updateRecord(currentCategory);
                Notification.showNotification(General.RECORD_SAVED_SUCCESS, "");
                    System.out.println(General.RECORD_SAVED_SUCCESS);
                }
                else{
                    categoryBo.addRecord(currentCategory);
                Notification.showNotification(General.RECORD_SAVED_SUCCESS, "");
                System.out.println(General.RECORD_SAVED_SUCCESS);
                }
            }
            else {
                callback = callback.replaceFirst("SUCCESS", "");
                FacesContext.getCurrentInstance().validationFailed();
                FacesContext.getCurrentInstance().addMessage("categoryMessage", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        callback, ""));
            }
    }
    public void clearCategory(){
        currentCategory=new Category();
    }

    public Category getCurrentCategory() {
        return currentCategory;
    }

    public void setCurrentCategory(Category currentCategory) {
        this.currentCategory = currentCategory;
    }

    public List<Category> getListOfAllCateory() {
        return listOfAllCateory;
    }

    public void setListOfAllCateory(List<Category> listOfAllCateory) {
        this.listOfAllCateory = listOfAllCateory;
    }

    public Category_DAO getCategoryBo() {
        return categoryBo;
    }

    public void setCategoryBo(Category_DAO categoryBo) {
        this.categoryBo = categoryBo;
    }
    
}
