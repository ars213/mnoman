/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Setup;

import DAOS.Category_DAO;
import DAOS.Details_DAO;
import DAOS.Picture_DAO;
import DAOS.Products_DAO;
import Entities.Category;
import Entities.Details;
import Entities.Picture;
import Entities.Products;
import Util.SystemSetting;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.model.DefaultStreamedContent;

/**
 *
 * @author Raghad
 */
@ManagedBean
@SessionScoped
public class ProductDefinition {

    Products currentProducts = new Products();
    Products_DAO productsBO = new Products_DAO();
    List<Products> listOfAllProduct = new ArrayList<>();
    String searchName;
    Picture currentPic = new Picture();
    Details currentDetails = new Details();
    List<Category> listOfAllCateory = new ArrayList<>();

    List<Details> listOfAllDetails = new ArrayList<>();
    List<Picture> listOfAllPicture = new ArrayList<>();
    Map<String, String> CategoryMap;

    public void init() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        currentProducts = new Products();
        currentPic = new Picture();
        currentDetails = new Details();
        productsBO = new Products_DAO();
        listOfAllProduct = new Products_DAO().getList();
        listOfAllCateory = new Category_DAO().getList();
        System.out.println(listOfAllCateory.size());
        listOfAllDetails = new ArrayList<>();
        listOfAllPicture = new ArrayList<>();
        CategoryMap = new HashMap<String, String>();
        for (Category cat : listOfAllCateory) {
            CategoryMap.put(cat.getName(), cat.getRowId());
        }

    }

    public void clearProduct() {
        currentProducts = new Products();
        listOfAllDetails = new ArrayList<>();
        listOfAllPicture = new ArrayList<>();
    }

    public void onAddNewDeatils() {
        Details deta = new Details();
        listOfAllDetails.add(deta);
    }

    public void removeDeatilsRow(RowEditEvent event) {
        Details deta = new Details();
        deta = (Details) event.getObject();
        listOfAllDetails.remove(deta);

    }

    public void onAddNewPic() {
        Picture pic = new Picture();
        listOfAllPicture.add(pic);
    }

    public void removePicRow(RowEditEvent event) {
        Picture pic = new Picture();
        pic = (Picture) event.getObject();
        listOfAllPicture.remove(pic);

    }

    public void saveMoreDetails() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {

        if (listOfAllDetails.size() > 0) {
            for (Details details : listOfAllDetails) {
                if (details.getRowId() != null) {
                    // details.setProductId(currentProducts.getRowId());
                    System.out.println("upadte details");
                    Details_DAO dd = new Details_DAO();
                    dd.updateRecord(details);
                } else {
                    System.out.println("add new details");
                    details.setProductId(currentProducts.getRowId());
                    Details_DAO dd = new Details_DAO();
                    dd.addRecord(details);
                }
            }
        }
        if (listOfAllPicture.size() > 0) {
            for (Picture pic : listOfAllPicture) {
                if (pic.getRowId() != null) {
                    System.out.println("upadte pic");
                    //pic.setProductId(currentProducts.getRowId());
                    new Picture_DAO().updateRecord(pic);
                } else {
                    System.out.println("new pic");
                    pic.setProductId(currentProducts.getRowId());
                    new Picture_DAO().addRecord(pic);
                }
            }
        }

    }

    public void saveProduct() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        currentProducts.setStatus("Not Empey Product");
        String callback = productsBO.validateRecord(currentProducts);
        if (callback.equals(SystemSetting.STATUS_SUCCESS)) {
            if (currentProducts.getRowId() != null) {
                productsBO.updateRecord(currentProducts);
                saveAttachment();
            } else {
                productsBO.addRecord(currentProducts);
                saveAttachment();
            }
        }
        //currentProducts.getCategory_idObject();
//        
    }

    public void viewDetails() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        listOfAllDetails = new Details_DAO().getList("Product_Id = '" + currentProducts.getRowId() + "'");
        listOfAllPicture = new Picture_DAO().getList("Product_Id = '" + currentProducts.getRowId() + "'");
    }

    public void search() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        listOfAllProduct = productsBO.getList("Upper(NAME) LIKE Upper('%" + searchName + "%')");
        System.out.println(listOfAllProduct.size());
        searchName = "";
    }
    File f = null;

    String doc_name = "";
    DefaultStreamedContent streamed_current_attachment = new DefaultStreamedContent();
    InputStream inStream = null;
// Products p = new Products();

    public void uploadAttachment(FileUploadEvent event) {
        System.out.println("get in upload");
        try {
            System.out.println("get in try upload");
            //create directory for the profile picture inside the profile.
            f = new File("approval_inbox_attachment/");
            if (!f.isDirectory()) {
                f.mkdir();
            }
            String[] name = event.getFile().getFileName().split("\\.");
            String type = name[1];
            currentProducts = new Products();
            currentProducts.setPictuer(event.getFile().getFileName());
            inStream = event.getFile().getInputstream();

//            saving temp file
            //f = new File("temp_" + ApplicationController.getLoggedInUser().getPar_emp_id() + "." + type
            //);
            FileOutputStream outStream = new FileOutputStream(f);
            byte[] buffer = new byte[1024];
            int length;
            //copy the file content in bytes
            while ((length = inStream.read(buffer)) > 0) {
                outStream.write(buffer, 0, length);
            }
            outStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    Products_DAO pBO = new Products_DAO();

    public void saveAttachment() {
        try {

            f = new File("approval_inbox_attachment/" + currentProducts.getRowId() + "." + currentProducts.getPictuer().split("\\.")[1]);
            FileOutputStream outStream = new FileOutputStream(f);
            byte[] buffer = new byte[1024];
            int length;
            //copy the file content in bytes
            while ((length = inStream.read(buffer)) > 0) {
                outStream.write(buffer, 0, length);
            }
            inStream.close();
            outStream.close();
            pBO.updateRecord(currentProducts);
            f = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Products getCurrentProducts() {
        return currentProducts;
    }

    public void setCurrentProducts(Products currentProducts) {
        this.currentProducts = currentProducts;
    }

    public Products_DAO getProductsBO() {
        return productsBO;
    }

    public void setProductsBO(Products_DAO productsBO) {
        this.productsBO = productsBO;
    }

    public List<Products> getListOfAllProduct() {
        return listOfAllProduct;
    }

    public void setListOfAllProduct(List<Products> listOfAllProduct) {
        this.listOfAllProduct = listOfAllProduct;
    }

    public Picture getCurrentPic() {
        return currentPic;
    }

    public void setCurrentPic(Picture currentPic) {
        this.currentPic = currentPic;
    }

    public Details getCurrentDetails() {
        return currentDetails;
    }

    public void setCurrentDetails(Details currentDetails) {
        this.currentDetails = currentDetails;
    }

    public List<Category> getListOfAllCateory() {
        return listOfAllCateory;
    }

    public void setListOfAllCateory(List<Category> listOfAllCateory) {
        this.listOfAllCateory = listOfAllCateory;
    }

    public Map<String, String> getCategoryMap() {
        return CategoryMap;
    }

    public void setCategoryMap(Map<String, String> CategoryMap) {
        this.CategoryMap = CategoryMap;
    }

    public List<Details> getListOfAllDetails() {
        return listOfAllDetails;
    }

    public void setListOfAllDetails(List<Details> listOfAllDetails) {
        this.listOfAllDetails = listOfAllDetails;
    }

    public List<Picture> getListOfAllPicture() {
        return listOfAllPicture;
    }

    public void setListOfAllPicture(List<Picture> listOfAllPicture) {
        this.listOfAllPicture = listOfAllPicture;
    }

}
